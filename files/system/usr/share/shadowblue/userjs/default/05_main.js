/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Portions of this file are copyright of their respective authors and were originally released under the MIT license:
 * - Betterfox, Copyright © 2020 yokoffing. For licensing see LICENSE.Betterfox file
 *
 * All license files can be found in the top directory of the source code or in /usr/share/licenses/shadowblue in the built images.
 */

/****************************************************************************************
 * Smoothfox OPTION: NATURAL SMOOTH SCROLLING V3 [MODIFIED]                                      *
****************************************************************************************/
// credit: https://github.com/AveYo/fox/blob/cf56d1194f4e5958169f9cf335cd175daa48d349/Natural%20Smooth%20Scrolling%20for%20user.js
// recommended for 120hz+ displays
// largely matches Chrome flags: Windows Scrolling Personality and Smooth Scrolling
user_pref("apz.overscroll.enabled", true); // DEFAULT NON-LINUX
user_pref("general.smoothScroll", true); // DEFAULT
user_pref("general.smoothScroll.msdPhysics.continuousMotionMaxDeltaMS", 12);
user_pref("general.smoothScroll.msdPhysics.enabled", true);
user_pref("general.smoothScroll.msdPhysics.motionBeginSpringConstant", 600);
user_pref("general.smoothScroll.msdPhysics.regularSpringConstant", 650);
user_pref("general.smoothScroll.msdPhysics.slowdownMinDeltaMS", 25);
user_pref("general.smoothScroll.msdPhysics.slowdownMinDeltaRatio", "2");
user_pref("general.smoothScroll.msdPhysics.slowdownSpringConstant", 250);
user_pref("general.smoothScroll.currentVelocityWeighting", "1");
user_pref("general.smoothScroll.stopDecelerationWeighting", "1");
user_pref("mousewheel.default.delta_multiplier_y", 300); // 250-400; adjust this number to your liking

// PREF: restore login manager
user_pref("signon.rememberSignons", true);

// PREF: restore address and credit card manager
user_pref("extensions.formautofill.addresses.enabled", true);
user_pref("extensions.formautofill.creditCards.enabled", true);

// PREF: allow websites to ask you for your location
user_pref("permissions.default.geo", 0);

// PREF: allow websites to ask you to receive site notifications
user_pref("permissions.default.desktop-notification", 0);

// PREF: restore Top Sites on New Tab page
user_pref("browser.newtabpage.activity-stream.feeds.topsites", true); // Shortcuts

// PREF: remove sponsored content on New Tab page
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false); // Sponsored shortcuts
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false); // Recommended by Pocket
user_pref("browser.newtabpage.activity-stream.showSponsored", false); // Sponsored Stories

// PREF: remove default Top Sites (Facebook, Twitter, etc.)
// [NOTE] This does not block you from adding your own.
user_pref("browser.newtabpage.activity-stream.default.sites", "");

// PREF: restore search engine suggestions
user_pref("browser.search.suggest.enabled", true);

// PREF: enable container tabs
user_pref("privacy.userContext.enabled", true);

// PREF: enable hardware video decoding
user_pref("media.ffmpeg.vaapi.enabled", true);

// PREF: don't load pinned tabs on browser launch
user_pref("browser.sessionstore.restore_pinned_tabs_on_demand", true); // https://bugzilla.mozilla.org/show_bug.cgi?id=1693007

// PREF: set DoH provider
user_pref("network.trr.uri", "https://freedns.controld.com/no-ads-typo-malware");
// PREF: enforce DNS-over-HTTPS (DoH) but fallback to ISP's DNS if there any issues
user_pref("network.trr.mode", 2);
user_pref("network.trr.max-fails", 5);

// See: https://github.com/yokoffing/Betterfox/wiki/Optional-Hardening#fingerprinting
user_pref("privacy.fingerprintingProtection", true);
user_pref("privacy.resistFingerprinting.pbmode", true);

// Enable PipeWire camera support
// See: https://jgrulich.cz/2024/08/19/making-pipewire-default-option-for-firefox-camera-handling/
user_pref("media.webrtc.camera.allow-pipewire", true);

// Firefox GNOME Theme
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true); // Enable customChrome.css
user_pref("layers.acceleration.force-enabled", true); // Fix CSD sharp corners
user_pref("browser.uidensity", 0); // Set UI density to normal
user_pref("svg.context-properties.content.enabled", true); // Enable SVG context-propertes
user_pref("gnomeTheme.hideWebrtcIndicator", true); // Hide redundant WebRTC indicator since GNOME provides their own privacy icons in the top right
