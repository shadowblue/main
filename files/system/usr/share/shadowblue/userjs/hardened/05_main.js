/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Portions of this file are copyright of their respective authors and were originally released under the MIT license:
 * - Betterfox, Copyright © 2020 yokoffing. For licensing see LICENSE.Betterfox file
 *
 * All license files can be found in the top directory of the source code or in /usr/share/licenses/shadowblue in the built images.
 */

// PREF: disable search engine suggestions
user_pref("browser.search.suggest.enabled", false);

// PREF: enable HTTPS-Only Mode
// Warn me before loading sites that don't support HTTPS
// in both Normal and Private Browsing windows.
user_pref("dom.security.https_only_mode", true);
user_pref("dom.security.https_only_mode_error_page_user_suggestions", true);

// PREF: enforce certificate pinning
// [ERROR] MOZILLA_PKIX_ERROR_KEY_PINNING_FAILURE
user_pref("security.cert_pinning.enforcement_level", 2);

// PREF: enforce DNS-over-HTTPS (DoH)
user_pref("network.trr.mode", 3);
user_pref("network.dns.skipTRR-when-parental-control-enabled", false);

// PREF: disable all DRM content
user_pref("media.eme.enabled", false);
// PREF: hide the setting; this also disables the DRM prompt (optional)
user_pref("browser.eme.ui.enabled", false);

// PREF: require safe SSL negotiation
// [ERROR] SSL_ERROR_UNSAFE_NEGOTIATION
user_pref("security.ssl.require_safe_negotiation", true);

// See: https://github.com/yokoffing/Betterfox/wiki/Optional-Hardening#fingerprinting
user_pref("privacy.fingerprintingProtection", true);
user_pref("privacy.resistFingerprinting", true);
user_pref("privacy.resistFingerprinting.letterboxing", true);
user_pref("privacy.resistFingerprinting.block_mozAddonManager", true);