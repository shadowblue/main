#!/usr/bin/env bash
# SPDX-FileCopyrightText: Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>
#
# SPDX-License-Identifier: Apache-2.0
#
# Portions of this file are copyright of their respective authors and were originally released under the MIT license:
# - Minimal safe Bash script template, Copyright © 2020 Maciej Radzikowski. For licensing see LICENSE.BashScriptTemplate file
#
# All license files can be found in the top directory of the source code or in /usr/share/licenses/shadowblue in the built images.

set -Eeuo pipefail

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [--hardened true|false] [--gnome-theme true|false] [-p profile_dir_name] [-a] [-y]
Enable user.js file generation with shadowblue defaults (based on Betterfox) and Firefox GNOME theme for selected Firefox profiles.
Available options:
-h, --help                          Print this help and exit
-v, --verbose                       Print script debug info
    --hardened  true|false          Choose to use hardened user.js or not
    --gnome-theme  true|false       Choose to use Firefox GNOME theme or not
-p, --profile  profile_dir_name     Choose specific profile
-a, --all                           Enable user.js for all profiles
-y, --yes                           Hide backup warning
EOF
  exit
}

# shellcheck disable=SC2034
setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "${RED}${msg}${NOFORMAT}"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  hardened=notselected
  gnome_theme=notselected
  profile=''
  all=0
  yes=0

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    --no-color) NO_COLOR=1 ;;
    --hardened)
      hardened="${2-}"
      shift
      ;;
    --gnome-theme)
      gnome_theme="${2-}"
      shift
      ;;
    -p | --profile)
      profile="${2-}"
      shift
      ;;
    -a | --all) all=1 ;;
    -y | --yes) yes=1 ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  return 0
}

enable_profile() {
  profiles=$1

  for profile in $profiles; do
    systemctl --user enable --now "${unit}${profile}.service"
    enable_gnome_theme "$profile"
  done
}

enable_gnome_theme() {
  profile=$1

  if [[ "$gnome_theme" == true ]]; then
    systemctl --user enable --now "update-firefox-gnome-theme@${profile}.service"
  elif [[ "$gnome_theme" != false ]]; then
    gum confirm "Do you want to enable Firefox GNOME theme for profile ${profile}?" \
      && systemctl --user enable --now "update-firefox-gnome-theme@${profile}.service"
  fi
}

parse_params "$@"
setup_colors

if [[ yes -eq 0 ]]; then
  gum confirm --prompt.foreground="#FF0000" --default=No --affirmative="I'M SURE!" --negative="Let me create a backup" \
    'THIS ACTION IS IRREVERSIBLE!!! You cant rollback these tweaks later! Please make sure you have a backup of your Firefox profile!' || \
    (gum style \
      --foreground 212 --border-foreground 212 --border double \
      --align center --width 80 --margin "1 2" --padding "2 4" \
      'Firefox profiles directories are stored in ~/.var/app/org.mozilla.firefox/.mozilla/firefox/' '' \
      'You can see profiles list in profiles.ini there or in about:profiles in your browser.' \
      'Choose directory to backup and make a copy of it!' && exit 0)
fi

profiles=$(grep 'Path=' ~/.var/app/org.mozilla.firefox/.mozilla/firefox/profiles.ini | cut -d'=' -f2)

if [[ "$hardened" == true ]]; then
  unit="update-userjs-hardened@"
elif [[ "$hardened" == false ]]; then
  unit="update-userjs@"
else
  gum confirm --default=No "Do you want to use hardened version of user.js? (Not recommended for newbies as some things in Firefox will not work by default. E.g. canvas)" \
    && unit="update-userjs-hardened@" || unit="update-userjs@"
fi

if [[ -n "$profile" ]]; then
  enable_profile "$profile"
elif [[ all -eq 1 ]]; then
  enable_profile "$profiles"
else
  profiles=$(echo "$profiles" | gum choose --no-limit --header="Choose which Firefox profiles to change:")
  enable_profile "$profiles"
fi
