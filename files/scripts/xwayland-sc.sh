#!/usr/bin/env bash
# SPDX-FileCopyrightText: Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>
#
# SPDX-License-Identifier: Apache-2.0

set -Eeuo pipefail

# dnf copr doesn't work on F40 or systems with rpm-ostree cliwrap enabled
if [[ $(rpm -E %fedora) -le 40 ]] || [[ -d /usr/libexec/rpm-ostree/wrapped ]]; then
    wget -O /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:shdwchn10:xwayland-sc.repo \
        "https://copr.fedorainfracloud.org/coprs/shdwchn10/xwayland-sc/repo/fedora-$(rpm -E %fedora)/shdwchn10-xwayland-sc-fedora-$(rpm -E %fedora).repo"
else
    dnf copr enable -y shdwchn10/xwayland-sc
fi

rpm-ostree override replace --experimental --freeze --from repo='copr:copr.fedorainfracloud.org:shdwchn10:xwayland-sc' xorg-x11-server-Xwayland
