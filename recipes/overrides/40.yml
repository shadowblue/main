# SPDX-FileCopyrightText: Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>
#
# SPDX-License-Identifier: Apache-2.0

name: main
description: A shadowblue image with some batteries for general desktop use.

base-image: IMAGE_REF
image-version: IMAGE_TAG

modules:
  - from-file: common.yml

  - type: containerfile
    snippets:
      - COPY --from=quay.io/shadowblue/AKMODS_VARIANT:IMAGE_TAG /akmods /tmp/akmods
      - RUN rpm-ostree install /tmp/akmods/*xpadneo*.rpm

  - type: script
    scripts:
      - xwayland-sc.sh

  - type: files
    files:
      - source: pre-f41
        destination: /

  # GNOME Extensions
  # Why not use a package manager? Some of these extensions unavailable in Fedora repos and some of these don't get updates quick.
  # Why even install any extensions by default? All of them actually improve default GNOME UX
  # and I believe that they do it without conflicting with GNOME philosophy (with exception of Appindicator Support).
  - type: gnome-extensions
    install:
      # Always Indicator. Simple extension which always shows indicator near clock if there are notifications.
      - always-indicatormartin.zurowietz.de.v17 # https://extensions.gnome.org/extension/2594/always-indicator/
      # Legacy tray support. Tray sucks but there are still many apps using it: Steam, Discord, Element, etc.
      - appindicatorsupportrgcjonas.gmail.com.v59 # https://extensions.gnome.org/extension/615/appindicator-support/
      # Caffeine can be useful when you have to stare at non-fullscreen static content.
      - caffeinepatapon.info.v56 # https://extensions.gnome.org/extension/517/caffeine/
      # Allows to use ddcutil for changing the brightness of external displays
      - display-brightness-ddcutilthemightydeity.github.com.v54 # https://extensions.gnome.org/extension/2645/brightness-control-using-ddcutil/
      # Fuzzy application search in GNOME Overview. Finds apps even with typos.
      - gnome-fuzzy-app-searchgnome-shell-extensions.Czarlie.gitlab.com.v25 # https://extensions.gnome.org/extension/3956/gnome-fuzzy-app-search/
      # Hot Edge is actually a lot better than hot corner.
      - hotedgejonathan.jdoda.ca.v26 # https://extensions.gnome.org/extension/4222/hot-edge/
      # Pano is the best clipboard manager. Clipboard history is very useful for many types of workflows.
      # BROKEN: No GNOME 46 support
      # - panoelhan.io.v22 # https://extensions.gnome.org/extension/5278/pano/
      # Simple tiling. It doesn't break default windowing logic, only extends it.
      - tiling-assistantleleat-on-github.v48 # https://extensions.gnome.org/extension/3733/tiling-assistant/
      # PiP on top. Makes "Picture-in-Picture" windows stay on top (even on Wayland session)
      - pip-on-toprafostar.github.com.v10 # https://extensions.gnome.org/extension/4691/pip-on-top/
      # Transparent Top Bar. This functionality will likely be added to the GNOME upstream.
      - transparent-top-barzhanghai.me.v24 # https://extensions.gnome.org/extension/1708/transparent-top-bar/

  - type: gschema-overrides
    include:
      - zz1-shadowblue-main-mutter.gschema.override
